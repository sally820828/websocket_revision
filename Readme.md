# 目錄

  I. [程式概述](#i-程式概述)

  II. [程式結構](#ii-程式結構)

  III. [環境建置](#iii-環境建置)

  IV. [容器執行](#iv-容器執行)
  

##  I. 程式概述

* 會員系統結合即時聊天室功能

##  II. 程式結構

* 程式結構:
![Alt text](image-1.png) 

##  III. 環境建置

* **環境需求:**
  Java - JDK17 
  MSSQL
  Docker - Image: mcr.microsoft.com/mssql/server:latest
         - Image: khipu/openjdk17-alpine

## IV. 容器執行

開啟Powershell

依序執行指令碼: 
* Build + Run 
```
docker-compose up -d
```
* 複製MSSQL備份

```
cp 'Path/mli_project.bak' withvolume-mssql:/var/opt/mssql/data/

範例: 
cp 'C:/job/005-Java-WebSocket/002-Docker/mli_project.bak' withvolume1-mssql-1:/var/opt/mssql/data/
```

* 啟動Bash
```
docker exec -it withvolume-mssql bash
```

* 還原備份

```
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'Ji394sure00' -Q "RESTORE DATABASE [mli_project] FROM DISK = '/var/opt/mssql/data/mli_project.bak' WITH MOVE 'mli_project' TO '/var/opt/mssql/data/mli_project.mdf', MOVE 'mli_project_log' TO '/var/opt/mssql/data/mli_project_log.ldf'"
```

* 以上完成, 啟動專案即可. 


* 測試:
刪掉Container

```
docker stop withvolume1-websocket-1
docker stop withvolume1-mssql-1
docker rm withvolume1-websocket-1
docker rm withvolume1-mssql-1
```

重新build + run

```
docker-compose up
```

進入bash查詢資料看剛才異動是否持久化

```
docker exec -it withvolume1-mssql-1 bash

/opt/mssql-tools/bin/sqlcmd -S localhost -U SA

use mli_project
select * from user_info
go
```
