package com.mli.websocket.user.model;

import java.time.LocalDateTime;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Table `user_info`
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.09
 */
public class UserInfo {

	private Integer userNo;

	private String userAccount;
	private String userPassword;

	private String userNickname;
	private String userGender;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date userBirthday;
	private String userPhone;
	private String userEmail;
	private byte[] userPhoto;
	private String userStatus;

	private String creator;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime creationTime;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime latestEditedTime;

	/**
	 * Getter & Setter
	 */

	public Integer getUserNo() {
		return userNo;
	}

	public void setUserNo(Integer userNo) {
		this.userNo = userNo;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserNickname() {
		return userNickname;
	}

	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}

	public String getUserGender() {
		return userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	public Date getUserBirthday() {
		return userBirthday;
	}

	public void setUserBirthday(Date userBirthday) {
		this.userBirthday = userBirthday;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public byte[] getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(byte[] userPhoto) {
		this.userPhoto = userPhoto;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getLatestEditedTime() {
		return latestEditedTime;
	}

	public void setLatestEditedTime(LocalDateTime latestEditedTime) {
		this.latestEditedTime = latestEditedTime;
	}
	

	public UserInfo(Integer userNo, String userAccount, String userPassword, String userNickname, String userGender,
			Date userBirthday, String userPhone, String userEmail, byte[] userPhoto, String userStatus, String creator,
			LocalDateTime creationTime, LocalDateTime latestEditedTime) {
		this.userNo = userNo;
		this.userAccount = userAccount;
		this.userPassword = userPassword;
		this.userNickname = userNickname;
		this.userGender = userGender;
		this.userBirthday = userBirthday;
		this.userPhone = userPhone;
		this.userEmail = userEmail;
		this.userPhoto = userPhoto;
		this.userStatus = userStatus;
		this.creator = creator;
		this.creationTime = creationTime;
		this.latestEditedTime = latestEditedTime;
	}

	/**
	 * Convert to String
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserInfo [userNo=");
		builder.append(userNo);
		builder.append(", userAccount=");
		builder.append(userAccount);
		builder.append(", userPassword=");
		builder.append(userPassword);

		builder.append(", userNickname=");
		builder.append(userNickname);
		builder.append(", userGender=");
		builder.append(userGender);
		builder.append(", userPhone=");
		builder.append(userPhone);
		builder.append(", userEmail=");
		builder.append(userEmail);

		builder.append(", userPhoto=");
		builder.append(userPhoto);
		builder.append(", userStatus=");
		builder.append(userStatus);

		builder.append(", creator=");
		builder.append(creator);
		builder.append(", creationTime=");
		builder.append(creationTime);
		builder.append(", latestEditedTime=");
		builder.append(latestEditedTime);
		builder.append("]");
		return builder.toString();
	}

	public UserInfo() {
	}

}

//Create table user_info(
//user_no INT NOT NULL PRIMARY KEY IDENTITY(1,1),
//user_account VARCHAR(10) NOT NULL UNIQUE,
//user_password VARCHAR(255) NOT NULL,
//
//user_nickname NVARCHAR(20) NOT NULL,
//user_gender NVARCHAR(10) NOT NULL,
//user_birthday DATETIME2 NOT NULL,
//user_phone VARCHAR(15) NOT NULL,
//user_email VARCHAR(50) NOT NULL UNIQUE,
//user_photo VARBINARY(MAX),
//user_status NVARCHAR(10) NOT NULL,
//
//creator NVARCHAR(10) NOT NULL,
//creation_time DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP,
//latest_edited_time DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP
//);