package com.mli.websocket.user.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mli.websocket.config.RandomStringGenerator;
import com.mli.websocket.user.model.UserInfo;
import com.mli.websocket.user.service.MailService;
import com.mli.websocket.user.service.UserService;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;

import jakarta.servlet.http.HttpSession;

/**
 * Manage database through the controller
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.09.21
 */
@RequestMapping("userInfo")
@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private MailService mailService;

	private final Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * 
	 * 
	 * */

	/**
	 * Sign up Page
	 */
	@GetMapping("/signupPage")
	public String signupPage() {
		return "userInfo/signupPage";
	}

	/**
	 * Sign up process
	 */
	@PostMapping("/signupProcess")
	public String signupProcess(@ModelAttribute("userInfo") UserInfo userInfo, Model model, HttpSession session) {

		Integer userNo = (Integer) session.getAttribute("userNo");

		if (userNo == null) {
			logger.error("Please logout first!");
			return "userInfo/loginPage";
		}

		if (userInfo != null) {

			UserInfo insertUser = userService.insertUser(userInfo);

			String userEmail = userInfo.getUserEmail();
			String userNickname = userInfo.getUserNickname();

			String title = "Sign up Successfully! ";
			String message = "Dear " + userNickname + "\n" + "Thank you for signup! ";
			mailService.sendEmail(userEmail, title, message);

			session.setAttribute("insertUser", insertUser);
			return "redirect:/userInfo/loginPage";
		}
		return null;
	}

	/**
	 * Enter Login Page
	 */
	@GetMapping("/loginPage")
	public String loginPage(HttpSession session, Model model) {
		String captcha = RandomStringGenerator.getRandomStringByLength(4);
		session.setAttribute("captcha", captcha);
		model.addAttribute("firstCaptcha", captcha);
		return "userInfo/loginPage";
	}

	/**
	 * Change captcha 亂數驗證
	 */
	@ResponseBody
	@GetMapping("/changeCapcha")
	public String changeCapcha(HttpSession session) {
		String captcha = RandomStringGenerator.getRandomStringByLength(4);
		session.setAttribute("captcha", captcha);
		return captcha;
	}

	/**
	 * Login - Check the userNo & userPassword with database
	 */
	@PostMapping("/loginProcess")
	public String loginProcess(@RequestParam("userAccount") String userAccount,
			@RequestParam("userPassword") String userPassword, @RequestParam("captcha") String insertCaptcha,
			HttpSession session, Model model) {

		UserInfo user = userService.findUserByAccountPassword(userAccount, userPassword);
		String captcha = (String) session.getAttribute("captcha");

		if (user != null && captcha.equals(insertCaptcha)) {
			session.setAttribute("user", user);
			Integer userNo = user.getUserNo();
			session.setAttribute("userNo", userNo);

			String userNickname = user.getUserNickname();
			session.setAttribute(userNickname, userNickname); // For聊天室用
			session.setAttribute("senderRole", SenderRole.USER);

			model.addAttribute("user", user);
			return "userInfo/loginSuccess";
		} else {
			return "redirect:/userInfo/loginPage";
		}
	}

	/**
	 * Logout
	 */
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:loginPage";
	}

	/**
	 * Forgot Password Page
	 */
	@GetMapping("/forgetPasswordPage")
	public String forgetPasswordPage(HttpSession session, Model model) {
		String captcha = RandomStringGenerator.getRandomStringByLength(4);
		session.setAttribute("captcha", captcha);
		model.addAttribute("firstCaptcha", captcha);
		return "userInfo/forgetPassword";
	}

	/**
	 * Forget Password -> send mail
	 */
	@PostMapping("/forgetPasswordMail")
	public String sendEmail(@RequestParam("userAccount") String userAccount,
			@RequestParam("captcha") String insertCaptcha, HttpSession session, Model model) {

		String captcha = (String) session.getAttribute("captcha");

		if (!captcha.equals(insertCaptcha)) {
			return "redirect:/userInfo/loginPage?error=true";
		}

		String userPassword = userService.forgotPassword(userAccount).getUserPassword();
		String userEmail = userService.forgotPassword(userAccount).getUserEmail();
		String userNickname = userService.forgotPassword(userAccount).getUserNickname();

		String title = "Forgot Password";
		String message = "Dear " + userNickname + "\n" + "Your Password is " + userPassword;
		mailService.sendEmail(userEmail, title, message);

		return "userInfo/forgetPassword";
	}

	/**
	 * Read one row
	 * 
	 * @return UserInfo
	 */
	@GetMapping("/userSetting")
	public String userSetting(HttpSession session, Model model) {
		Integer userNo = (Integer) session.getAttribute("userNo");
		UserInfo findUserByNo = userService.findUserByNo(userNo);
		model.addAttribute("user", findUserByNo);
		return "userInfo/userEdit";
	}

	/**
	 * Edit
	 * 
	 * @return UserInfo
	 */
	@ResponseBody
	@PutMapping("/updateUserInfo")
	public UserInfo updateUserInfo(@RequestBody UserInfo userInfo, HttpSession session, Model model) {

		Integer userNo = (Integer) session.getAttribute("userNo");
		if (userNo == null) {
			logger.error("Please log in first");
		}
		try {
			UserInfo updateUserInfo = userService.updateUser(userInfo);
			model.addAttribute("updateUserInfo", updateUserInfo);
			return updateUserInfo;
		} catch (Exception e) {
			logger.error("Error");
			return null;
		}
	}

	// -------------------------------------------------------------------------------------

	/**
	 * manager Read all list
	 * 
	 * @return List<UserInfo>
	 */
	@ResponseBody
//	@Operation(summary = "Read all data", description = "Read all data from table `UserInfo`")
	@GetMapping("/listAllUserInfo")
	public String listAllUserInfo(HttpSession session, Model model) {
//		try {
		List<UserInfo> UserInfoList = userService.findAllUser();
		model.addAttribute("user", UserInfoList);
//			if(UserInfoList != null) {
//				logger.debug("Successful!");
		return "userInfo/userAlllist";
//			}else{
//				logger.debug("There is no files.");
//				return HttpStatus.NOT_FOUND;
//			}
//		} catch (Exception e) {
//			logger.error("Error! Failed to read table");
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//		}
	}

//	
	/**
	 * Edit
	 * 
	 * @return UserInfo
	 */
//	@ResponseBody
//	@PutMapping("/updateUserInfo")
//	public UserInfo updateUserInfo(@RequestBody UserInfo userInfo, HttpSession session, Model model) {
//		
//		Integer userNo = (Integer)session.getAttribute("userNo");
//		if(userNo==null) {
//			logger.error("Please log in first");
//		}
//		
//		try {
//		UserInfo updateUserInfo = userService.updateUser(userInfo);
//		model.addAttribute("updateUserInfo", updateUserInfo);
//		return updateUserInfo;
////			if(updateUserInfo != null) {
////				logger.debug("Successful!");
////				return ResponseEntity.ok(updateUserInfo);
////			}else {
////				logger.debug("the row is not exist");
////				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
////			}
////		} catch (Exception e) {
////			logger.error("Error");
////			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
////		}
//	}

	/**
	 * Edit status
	 * 
	 * @return UserInfo
	 */
//	@Operation(summary = "Edit one row", description = "Edit one row by userNo")
	@PutMapping("/updateUserStatus")
	public String updateUserInfo(@RequestParam("userNo") Integer userNo, @RequestParam("userStatus") String userStatus,
			HttpSession session, Model model) {
//		try {
		UserInfo updateUserInfo = userService.updateUserStatus(userNo, userStatus);
		model.addAttribute("user", updateUserInfo);
		return "redirect:/userInfo/userAlllist";
//			if(updateUserInfo != null) {
//				logger.debug("Successful!");
//				return ResponseEntity.ok(updateUserInfo);
//			}else {
//				logger.debug("the row is not exist");
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
//			}
//		} catch (Exception e) {
//			logger.error("Error");
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//		}
	}

	/**
	 * Delete one row
	 * 
	 * @return UserInfo
	 */
//	@Operation(summary = "Delete one row", description = "Delete one row by userNo")
	@DeleteMapping("/deleteUserInfo")
	public String deleteUserInfo(@RequestParam("userNo") Integer userNo, HttpSession session) {
//		try {
		userService.deleteUser(userNo);
		return "redirect:/userInfo/listAllUserInfo";
//			if(deleteUserInfo) {
//				logger.debug("Deleted successfully!");
//				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
//			}else {
//				logger.debug("the row is not exist");
//				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
//			}
//		} catch (Exception e) {
//			logger.error("Error");
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//		}
	}

}
