package com.mli.websocket.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mli.websocket.user.model.UserInfo;

/**
 * Mapper Interface for `user_info`
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.09
 */
@Mapper
public interface UserMapper {
	
	/**
	 * Sign up (User)
	 * @return UserInfo
	 */
	UserInfo insertUser(UserInfo userInfo);

	/**
	 * List all users (Manager)
	 * @return List<UserInfo> 
	 */
	List<UserInfo> findAllUser();
	
	/**
	 * Find one User 
	 * @return UserInfo
	 */
	UserInfo findUserByNo(Integer userNo);
	
	/**
	 * Login
	 * @return UserInfo
	 */
	UserInfo findUserByAccountPassword(String userAccount, String userPassword);
	
	/**
	 * Forget Password
	 * */
	UserInfo forgotPassword(String userAccount);
	
	/**
	 * Edit user's information
	 * @return UserInfo
	 */
	UserInfo updateUser(UserInfo userInfo);
	
	/**
	 * Edit account status
	 * @return 
	 * */
	UserInfo updateUserStatus(Integer userNo, String userStatus);
	
	/**
	 * Delete userInfo
	 * @return boolean
	 */
	boolean deleteUser(Integer userNo);
}
