package com.mli.websocket.user.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mli.websocket.user.mapper.UserMapper;
import com.mli.websocket.user.model.UserInfo;

/**
 * Service for manage table `UserInfo` in SQL Server
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.09
 */
@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	private Logger logger = LoggerFactory.getLogger(UserService.class);

	/**
	 * Insert one row (Sign up)
	 * 
	 * @return UserInfo
	 */
	public UserInfo insertUser(UserInfo UserInfo) {
		try {
			if (UserInfo == null) {
				logger.error("Please enter required information");
				return null;
			}
//			String encodePassword = passwordEncoder.encode(UserInfo.getUserPassword());
//			UserInfo.setUserPassword(encodePassword);
			UserInfo insertUser = userMapper.insertUser(UserInfo);
			logger.info("User inserted successfully: " + insertUser.toString());
			return insertUser;
		} catch (Exception e) {
			logger.error("Error occurred while inserting user: " + e.getMessage(), e);
		}
		return UserInfo;
	}

	/**
	 * Get all list
	 * @return List<UserInfo>
	 */
	public List<UserInfo> findAllUser() {
		try {
			List<UserInfo> allUserList = userMapper.findAllUser();
			if (allUserList != null) {
				logger.info("Find all UserList successfully!");
				return allUserList;
			} else {
				logger.error("The list is empty");
				return null;
			}
		} catch (Exception e) {
			logger.error("Error occurred while selecting all user: " + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Get one row
	 * @return UserInfo
	 */
	public UserInfo findUserByNo(Integer userNo) {
		try {
			if (userNo == null) {
				logger.error("UserNo is null");
				return null;
			}
			UserInfo queryUser = userMapper.findUserByNo(userNo);
			if (queryUser != null) {
				logger.info("UserInfo with UserNo: " + userNo + " exists");
				return queryUser;
			} else {
				logger.error("UserInfo with UserNo:" + userNo + " not found");
				return null;
			}
		} catch (Exception e) {
			logger.error("Error occurred while selecting one user: " + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 帳號密碼登入
	 */
	public UserInfo findUserByAccountPassword(String userAccount, String userPassword) {
		if (userAccount == null) {
			logger.error("UserAccount is null");
			return null;
		}
		if (userPassword == null) {
			logger.error("userPassword is null");
			return null;
		}
		try {
			UserInfo queryUser = userMapper.findUserByAccountPassword(userAccount, userPassword);
			if (queryUser == null) {
				logger.warn("User doesn't exist for given account and password");
			}
			logger.info("Find user info successfully!");
			return queryUser;
		} catch (Exception e) {
			logger.error("Error occurred while selecting one user by account & password: " + e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * Forget Password
	 */
	public UserInfo forgotPassword(String userAccount) {
		if(userAccount==null) {
			logger.error("userAccount is null");
		}
		UserInfo forgotPassword = userMapper.forgotPassword(userAccount);
		return forgotPassword;
	}

	/**
	 * Edit one row
	 * @return UserInfo
	 */
	public UserInfo updateUser(UserInfo userInfo) {
		if (userInfo == null) {
			logger.error("userInfo is null");
		}
		try {
			UserInfo updateUser = userMapper.updateUser(userInfo);
			if (updateUser != null) {
				logger.info("Successfully updated user info for userNo: " + userInfo.getUserNo());
				return userInfo;
			} else {
				logger.error("Failed to edit userInfo");
				return null;
			}
		} catch (Exception e) {
			logger.error("Error occurred while editing user:" + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Delete one row
	 * @return boolean
	 */
	public boolean deleteUser(Integer userNo) {
		if (userNo == null) {
			logger.error("UserNo is null");
			return false;
		}
		try {
			boolean deleteUser = userMapper.deleteUser(userNo);
			if (deleteUser) {
				logger.info("Successfully deleted user with UserNo: " + userNo);
			} else {
				logger.warn("Failed to delete user with UserNo: " + userNo);
			}
			return deleteUser;
		} catch (Exception e) {
			logger.error("Error occurred while deleting user with UserNo: " + userNo + ". Error: " + e.getMessage(), e);
			return false;
		}
	}

	/**
	 * Edit status
	 * 
	 * @return UserInfo
	 */
	public UserInfo updateUserStatus(Integer userNo, String userStatus) {
		UserInfo updateUser = userMapper.updateUserStatus(userNo, userStatus);
		if (updateUser != null) {
			return updateUser;
		} else {
			return null;
		}
	}
}
