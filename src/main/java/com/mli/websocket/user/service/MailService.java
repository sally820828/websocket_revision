package com.mli.websocket.user.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailService {

	private JavaMailSender mailSender;
	
	private Logger logger = LoggerFactory.getLogger(UserService.class);

	public MailService(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendEmail(String userEmail,
					      String title,
						  String message) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setTo(userEmail);
			messageHelper.setSubject(title);
			messageHelper.setText(message,true);
		};
		try {
			mailSender.send(messagePreparator);
			logger.info("Mail sent successfully!");
		} catch (MailException e) {
			
		}
	}

}