package com.mli.websocket.websocket.config;

/**
 * WebSocketEventListener
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.1.05
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.model.ClientMessage.MessageType;

public class WebSocketEventListener {
	
	private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);
	
	@Autowired
	private SimpMessageSendingOperations simpleMessageSendingOperations;
	
    //method called when user open page in browser
	@EventListener
	public void handleWebSocketConnectListener(SessionConnectedEvent connectedEvent) {
		
		logger.info("Received a new websocket conntection!");
	}
	
	
    //method called when user close page in browser
	@EventListener
	public void handleWebSocketDisconnectListener(SessionDisconnectEvent sessionDisconnectEvent) {
		StompHeaderAccessor stopmHeaderAccessor = StompHeaderAccessor.wrap(sessionDisconnectEvent.getMessage());
		
		String username = (String)stopmHeaderAccessor.getSessionAttributes().get("username");
		if(username != null) {
			logger.info("User Disconnected : " + username);
			
			ClientMessage clientMessage = new ClientMessage();
			clientMessage.setMessageType(MessageType.LEAVE);
			clientMessage.setSenderUserNickName(username);
			
			simpleMessageSendingOperations.convertAndSend("/chatroom/public",clientMessage);
		}
	}
}
