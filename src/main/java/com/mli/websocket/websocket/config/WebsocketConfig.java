package com.mli.websocket.websocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.mli.websocket.websocket.test.MyWebSocketHandler;

/**
 * WebSocketConfig
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.05
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer{

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.setApplicationDestinationPrefixes("/app");
		registry.enableSimpleBroker("/chatroom", "/user");
		registry.setUserDestinationPrefix("/user");
	}
//		 * Configure the prefix used to identify user destinations. User destinations
//		 * provide the ability for a user to subscribe to queue names unique to their
//		 * session as well as for others to send messages to those unique,
//		 * user-specific queues.

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/websocket")
				.setAllowedOriginPatterns("*")
				.addInterceptors(new HttpSessionHandshakeInterceptor()) 
				.withSockJS();
	}
	
	@Bean
	public WebSocketHandler myWebSocketHandler() {
	    return new MyWebSocketHandler();
	}
}

//* @param attributes the attributes from the HTTP handshake to associate with the WebSocket
//* session; the provided attributes are copied, the original map is not used.