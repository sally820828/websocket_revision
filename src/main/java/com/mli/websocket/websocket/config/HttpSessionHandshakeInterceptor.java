package com.mli.websocket.websocket.config;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import jakarta.servlet.http.HttpSession;

/**
 * HandshakeInterceptor for copy HttpSession to WebSocket
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.05
 */
public class HttpSessionHandshakeInterceptor implements HandshakeInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		if (request instanceof ServletServerHttpRequest) {
			ServletServerHttpRequest servletServerHttpRequest = (ServletServerHttpRequest) request;

			// Get Httpsession
			HttpSession httpSession = servletServerHttpRequest.getServletRequest().getSession();
			if (httpSession != null) {

				Integer userNo = (Integer) httpSession.getAttribute("userNo");
				String userNickname = (String) httpSession.getAttribute("userNickname");

				Integer managerId = (Integer) httpSession.getAttribute("managerId");
				String managerName = (String) httpSession.getAttribute("managerName");

				if (userNo != null && userNickname != null) {
					attributes.put("userNo", userNo);
					attributes.put("userNickname", userNickname);
				}
				if (managerId != null && managerName != null) {
					attributes.put("managerId", managerId);
					attributes.put("managerName", managerName);
				}
			} else {
				logger.info("HTTP Session is null");
			}
		}
		return true;
	}

//	* @param attributes the attributes from the HTTP handshake to associate with the WebSocket
//	 * session; the provided attributes are copied, the original map is not used.

	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception exception) {
	}
}
