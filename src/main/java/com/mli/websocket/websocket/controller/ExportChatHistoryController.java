package com.mli.websocket.websocket.controller;

import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.service.ExportExcelService;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * WebSocket Public Chat Room Controller
 * @author D2082803 Sally 
 * @version 1.0
 * @since 2023.10.12
 */
@Controller
@RequestMapping("websocket")
public class ExportChatHistoryController {

	@Autowired
	private ExportExcelService exportExcelService;

//	private final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	/**
	 * Export Chatting History
	 * */
	@GetMapping("/exportChattingHistroy")
	public void exportChattingHistroy(HttpServletResponse response, HttpSession httpSession) throws IOException {

		@SuppressWarnings("unchecked")
		List<ClientMessage> clientMessageList = (List<ClientMessage>) httpSession.getAttribute("listAllClientMessage");
		XSSFWorkbook exportChattingHistoryToExcel = exportExcelService.exportChattingHistoryToExcel(clientMessageList);

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=Chatting_History.xlsx");
		exportChattingHistoryToExcel.write(response.getOutputStream());
		exportChattingHistoryToExcel.close();
	}

}
