package com.mli.websocket.websocket.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mli.websocket.manager.model.Manager;
import com.mli.websocket.manager.service.ManagerService;
import com.mli.websocket.user.model.UserInfo;
import com.mli.websocket.user.service.UserService;
import com.mli.websocket.websocket.config.WebSocketEventListener;
import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.model.ClientMessage.ReceiverRole;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;
import com.mli.websocket.websocket.service.ClientMessageService;
import jakarta.servlet.http.HttpSession;

/**
 * WebSocket Public Chat Room Controller
 * @author D2082803 Sally 
 * @version 1.0
 * @since 2023.10.12
 */
@Controller
@RequestMapping("websocket")
public class PublicChatRoomController {

	@Autowired
	private UserService userService;

	@Autowired
	private ClientMessageService clientMessageService;

	@Autowired
	private ManagerService managerService;

	private final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	/**
	 * Entering into Main Page of chat room
	 */
	@GetMapping("/publicChatRoom")
	public String chatRoomPage(HttpSession httpSession, Model model) {

		Integer userNo = (Integer) httpSession.getAttribute("userNo");
		Integer managerId = (Integer) httpSession.getAttribute("managerId");

		if (userNo != null) {
			UserInfo userInfo = userService.findUserByNo(userNo);
			String userNickname = userInfo.getUserNickname();

			httpSession.setAttribute("userNickname", userNickname);
			httpSession.setAttribute("userNo", userNo);
			httpSession.setAttribute("senderRole", SenderRole.USER);

			model.addAttribute("userNo", userNo);
			model.addAttribute("userNickname", userNickname);

		} else if (managerId != null) {
			Manager manager = managerService.findManagerById(managerId);
			String managerName = manager.getManagerName();

			httpSession.setAttribute("managerName", managerName);
			httpSession.setAttribute("managerId", managerId);
			httpSession.setAttribute("senderRole", SenderRole.MANAGER);

			model.addAttribute("managerId", managerId);
			model.addAttribute("managerName", managerName);
		}
		return "websocket/publicChatRoom";
	}
	
	/**
	 * Add sender to chatroom
	 * */
	@MessageMapping("/messageAddSender") 
	@SendTo("/chatroom/public")
	public ClientMessage addUser(@Payload ClientMessage clientMessage, SimpMessageHeaderAccessor headerAccessor) {

		Integer userNo = (Integer) headerAccessor.getSessionAttributes().get("userNo");
		Integer managerId = (Integer) headerAccessor.getSessionAttributes().get("managerId");

		ClientMessage newClientMessage = new ClientMessage();
		newClientMessage.setMessageType(clientMessage.getMessageType());
		newClientMessage.setMessageDatetime(LocalDateTime.now());
		newClientMessage.setMessageContent(clientMessage.getMessageContent());
		newClientMessage.setReceiverRole(ReceiverRole.ALL); // All

		if (userNo != null) {
			UserInfo userInfo = userService.findUserByNo(userNo);
			String senderUserNickName = userInfo.getUserNickname();

			if (senderUserNickName != null) {
				headerAccessor.getSessionAttributes().put("username", senderUserNickName);
				headerAccessor.getSessionAttributes().put("userNo", userNo);
				logger.info("username" + senderUserNickName);
				logger.info("userNo" + userNo);
			} else {
				logger.info("username is null");
			}

			newClientMessage.setSenderRole(SenderRole.USER); // USER
			newClientMessage.setSenderUserNo(userNo);
			newClientMessage.setSenderUserNickName(senderUserNickName);

			clientMessageService.insertClientMessage(newClientMessage);

			SenderRole senderRole = newClientMessage.getSenderRole();
			Integer senderUserNo = newClientMessage.getSenderUserNo();

			headerAccessor.getSessionAttributes().put("senderRole", senderRole);
			headerAccessor.getSessionAttributes().put("senderUserNo", senderUserNo);

			return newClientMessage;

		} else if (managerId != null) {
			Manager manager = managerService.findManagerById(managerId);
			String senderUserNickName = manager.getManagerName();

			if (senderUserNickName != null) {
				headerAccessor.getSessionAttributes().put("username", senderUserNickName);
				headerAccessor.getSessionAttributes().put("managerId", managerId);
				logger.info("username" + senderUserNickName);
				logger.info("managerId" + managerId);
			} else {
				logger.info("username is null");
			}

			newClientMessage.setSenderRole(SenderRole.MANAGER);
			newClientMessage.setSenderManagerId(managerId);
			newClientMessage.setSenderUserNickName(senderUserNickName);

			clientMessageService.insertClientMessage(newClientMessage);

			SenderRole senderRole = newClientMessage.getSenderRole();
			Integer senderManagerId = newClientMessage.getSenderManagerId();

			headerAccessor.getSessionAttributes().put("senderRole", senderRole);
			headerAccessor.getSessionAttributes().put("senderManagerId", senderManagerId);
			return newClientMessage;
		}
		return newClientMessage;
	}
	
	/**
	 * Sending messages in public chat room
	 */
	@MessageMapping("/messageSendMessage") // /app/messageSendMessage
	@SendTo("/chatroom/public")
	public ClientMessage receiveClientMessage(@Payload ClientMessage clientMessage,
			SimpMessageHeaderAccessor headerAccessor) {

		SenderRole senderRole = (SenderRole) headerAccessor.getSessionAttributes().get("senderRole");
		
		if (senderRole.equals(SenderRole.USER)) {
			Integer senderUserNo = (Integer) headerAccessor.getSessionAttributes().get("senderUserNo");

			UserInfo userInfo = userService.findUserByNo(senderUserNo);
			String senderUserNickName = userInfo.getUserNickname();

			ClientMessage newClientMessage = new ClientMessage();
			newClientMessage.setMessageType(clientMessage.getMessageType());
			newClientMessage.setMessageDatetime(LocalDateTime.now());
			newClientMessage.setMessageContent(clientMessage.getMessageContent());

			newClientMessage.setSenderRole(SenderRole.USER);
			newClientMessage.setSenderUserNo(senderUserNo);
			newClientMessage.setSenderManagerId(clientMessage.getSenderManagerId());
			newClientMessage.setSenderUserNickName(senderUserNickName);

			newClientMessage.setReceiverRole(ReceiverRole.ALL);

			clientMessageService.insertClientMessage(newClientMessage);

			return newClientMessage;

		} else if (senderRole.equals(SenderRole.MANAGER)) {
			Integer senderManagerId = (Integer) headerAccessor.getSessionAttributes().get("senderManagerId");
			
			Manager manager = managerService.findManagerById(senderManagerId);
			String senderUserNickName = manager.getManagerName();
			
			ClientMessage newClientMessage = new ClientMessage();
			newClientMessage.setMessageType(clientMessage.getMessageType());
			newClientMessage.setMessageDatetime(LocalDateTime.now());
			newClientMessage.setMessageContent(clientMessage.getMessageContent());

			newClientMessage.setSenderRole(SenderRole.MANAGER);
			newClientMessage.setSenderManagerId(senderManagerId);
			newClientMessage.setSenderUserNickName(senderUserNickName);

			newClientMessage.setReceiverRole(ReceiverRole.ALL);

			clientMessageService.insertClientMessage(newClientMessage);

			return newClientMessage;
		}
		return null;

	}
	
	/**
	 * List all chat list
	 */
	@GetMapping("/listAllChattingMessages")
	public String listAllChattingMessages(HttpSession httpSession, Model model) {

		Integer managerId = (Integer)httpSession.getAttribute("managerId");
		if(managerId==null) {
			logger.error("managerId is null");
			return "manager/managerLoginPage";
		}
		List<ClientMessage> listAllClientMessage = clientMessageService.listAllClientMessage();
		model.addAttribute("listAllClientMessage", listAllClientMessage);
		httpSession.setAttribute("listAllClientMessage", listAllClientMessage);
		return "websocket/listChatHistory";
	}
	
	
}
