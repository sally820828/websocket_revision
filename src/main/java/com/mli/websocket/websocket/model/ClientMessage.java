package com.mli.websocket.websocket.model;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Table `client_message`
 * For saving messages in public chat room or private chatroom from client(user+manager)
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.03
 */
public class ClientMessage {

	private Integer messageId;
	private MessageType messageType;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime messageDatetime;
	private String messageContent;

	private SenderRole senderRole;
	private Integer senderUserNo;
	private Integer senderManagerId;
	private String senderUserNickName;

	private ReceiverRole receiverRole;
	private Integer receiverUserNo;
	private Integer receiverManagerId;

	public ClientMessage() {
	}

	/**
	 * Getter & Setter
	 */
	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public enum MessageType {
		CHAT, 
		JOIN, 
		LEAVE
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public LocalDateTime getMessageDatetime() {
		return messageDatetime;
	}

	public void setMessageDatetime(LocalDateTime messageDatetime) {
		this.messageDatetime = messageDatetime;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public enum SenderRole {
		USER, 
		MANAGER
	}

	public SenderRole getSenderRole() {
		return senderRole;
	}

	public void setSenderRole(SenderRole senderRole) {
		this.senderRole = senderRole;
	}

	public Integer getSenderUserNo() {
		return senderUserNo;
	}

	public void setSenderUserNo(Integer senderUserNo) {
		this.senderUserNo = senderUserNo;
	}

	public Integer getSenderManagerId() {
		return senderManagerId;
	}

	public void setSenderManagerId(Integer senderManagerId) {
		this.senderManagerId = senderManagerId;
	}

	public enum ReceiverRole {
		USER, 
		MANAGER, 
		ALL
	}

	public ReceiverRole getReceiverRole() {
		return receiverRole;
	}
	
	public void setReceiverRole(ReceiverRole receiverRole) {
		this.receiverRole = receiverRole;
	}

	public Integer getReceiverUserNo() {
		return receiverUserNo;
	}

	public void setReceiverUserNo(Integer receiverUserNo) {
		this.receiverUserNo = receiverUserNo;
	}

	public Integer getReceiverManagerId() {
		return receiverManagerId;
	}

	public void setReceiverManagerId(Integer receiverManagerId) {
		this.receiverManagerId = receiverManagerId;
	}

	public ClientMessage(Integer messageId, MessageType messageType, LocalDateTime messageDatetime,
			String messageContent, SenderRole senderRole, Integer senderUserNo, Integer senderManagerId,
			ReceiverRole receiverRole, Integer receiverUserNo, Integer receiverManagerId) {
		super();
		this.messageId = messageId;
		this.messageType = messageType;
		this.messageDatetime = messageDatetime;
		this.messageContent = messageContent;
		this.senderRole = senderRole;
		this.senderUserNo = senderUserNo;
		this.senderManagerId = senderManagerId;
		this.receiverRole = receiverRole;
		this.receiverUserNo = receiverUserNo;
		this.receiverManagerId = receiverManagerId;
	}

	public String getSenderUserNickName() {
		return senderUserNickName;
	}

	public void setSenderUserNickName(String senderUserNickName) {
		this.senderUserNickName = senderUserNickName;
	}

	/**
	 * Convert to String
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientMessage [messageId=");
		builder.append(messageId);
		builder.append(", messageType=");
		builder.append(messageType);
		builder.append(", messageDatetime=");
		builder.append(messageDatetime);
		builder.append(", messageContent=");
		builder.append(messageContent);
		builder.append(", senderRole=");
		builder.append(senderRole);
		builder.append(", senderUserNo=");
		builder.append(senderUserNo);
		builder.append(", senderManagerId=");
		builder.append(senderManagerId);
		builder.append(", receiverRole=");
		builder.append(receiverRole);
		builder.append(", receiverUserNo=");
		builder.append(receiverUserNo);
		builder.append(", receiverManagerId=");
		builder.append(receiverManagerId);
		builder.append("]");
		return builder.toString();
	}
}

//Create table client_message(
//message_id INT NOT NULL PRIMARY KEY IDENTITY(1,1),
//message_type NVARCHAR(10) NOT NULL,  -- public / private
//message_datetime DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP,
//message_content NVARCHAR(150) NOT NULL,
//
//sender_role NVARCHAR(10) NOT NULL, -- from manager or user or others
//sender_user_no INT FOREIGN KEY (sender_user_no) REFERENCES user_info(user_no),
//sender_manager_id INT FOREIGN KEY (sender_manager_id) REFERENCES manager(manager_id),
//
//receiver_role NVARCHAR(10) NOT NULL, -- user or manager or all(public)
//receiver_user_no INT FOREIGN KEY (receiver_user_no) REFERENCES user_info(user_no),
//receiver_manager_id INT FOREIGN KEY (receiver_manager_id) REFERENCES manager(manager_id)
//)
