package com.mli.websocket.websocket.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Table `manager_server_message`
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.03
 */
public class ServerMessage {
	
	private Integer messageId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date messageDatetime;
	private String messageType;
	private String messageContent;

	private Integer managerId;
	private String managerName;

	public ServerMessage() {
	}

	/**
	 * Getter & Setter
	 */
	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public Date getMessageDatetime() {
		return messageDatetime;
	}

	public void setMessageDatetime(Date messageDatetime) {
		this.messageDatetime = messageDatetime;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
	public ServerMessage(Integer messageId, Date messageDatetime, String messageType, String messageContent,
			Integer managerId, String managerName) {
		super();
		this.messageId = messageId;
		this.messageDatetime = messageDatetime;
		this.messageType = messageType;
		this.messageContent = messageContent;
		this.managerId = managerId;
		this.managerName = managerName;
	}

	/**
	 * Convert to String
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServerMessage [messageId=");
		builder.append(messageId);
		builder.append(", messageDatetime=");
		builder.append(messageDatetime);
		builder.append(", messageType=");
		builder.append(messageType);
		builder.append(", messageContent=");
		builder.append(messageContent);
		builder.append(", managerId=");
		builder.append(managerId);
		builder.append(", managerName=");
		builder.append(managerName);
		builder.append("]");
		return builder.toString();
	}
}

//
//Create table manager_server_message(
//message_id INT NOT NULL PRIMARY KEY IDENTITY(1,1),
//message_datetime DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP,
//message_type NVARCHAR(20) NOT NULL,
//message_content NVARCHAR(150) NOT NULL,
//manager_name NVARCHAR(10) NOT NULL,
//manager_id INT NOT NULL FOREIGN KEY (manager_id) REFERENCES manager(manager_id)
//)