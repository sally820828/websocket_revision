package com.mli.websocket.websocket.test;


/**
 * dto
 */
public class ClientMessageDto {

//	private Integer messageId;
	private MessageType messageType;

//	private String messageDatetime;
	private String messageContent;
//	private Integer senderUserNo;
	private String senderUserNickName;

//	private Integer receiverUserNo;
//	private String receiverNickName;
	
	public ClientMessageDto() {
	}

	/**
	 * Getter & Setter
	 */
//	public Integer getMessageId() {
//		return messageId;
//	}
//
//	public void setMessageId(Integer messageId) {
//		this.messageId = messageId;
//	}

	public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

	public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType type) {
        this.messageType = type;
    }

//	public String getMessageDatetime() {
//		return messageDatetime;
//	}
//
//	public void setMessageDatetime(String messageDatetime) {
//		this.messageDatetime = messageDatetime;
//	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

//	public Integer getSenderUserNo() {
//		return senderUserNo;
//	}
//
//	public void setSenderUserNo(Integer senderUserNo) {
//		this.senderUserNo = senderUserNo;
//	}

	public String getSenderUserNickName() {
		return senderUserNickName;
	}

	public void setSenderUserNickName(String senderUserNickName) {
		this.senderUserNickName = senderUserNickName;
	}

//	public Integer getReceiverUserNo() {
//		return receiverUserNo;
//	}
//
//	public void setReceiverUserNo(Integer receiverUserNo) {
//		this.receiverUserNo = receiverUserNo;
//	}
//
//	public String getReceiverNickName() {
//		return receiverNickName;
//	}
//
//	public void setReceiverNickName(String receiverNickName) {
//		this.receiverNickName = receiverNickName;
//	}

	/**
	 * Convert to String
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientMessageDto [messageId=");
//		builder.append(messageId);
		builder.append(", messageType=");
		builder.append(messageType);
//		builder.append(", messageDatetime=");
//		builder.append(messageDatetime);
		builder.append(", messageContent=");
		builder.append(messageContent);
//		builder.append(", senderUserNo=");
//		builder.append(senderUserNo);
		builder.append(", senderUserNickName=");
		builder.append(senderUserNickName);
//		builder.append(", receiverUserNo=");
//		builder.append(receiverUserNo);
//		builder.append(", receiverNickName=");
//		builder.append(receiverNickName);
		builder.append("]");
		return builder.toString();
	}

	public ClientMessageDto(
//			Integer messageId,
			MessageType messageType, 
//			String messageDatetime,
			String messageContent,
//			Integer senderUserNo,
			String senderUserNickName
//			Integer receiverUserNo, 
//			String receiverNickName
			) {
		super();
//		this.messageId = messageId;
		this.messageType = messageType;
//		this.messageDatetime = messageDatetime;
		this.messageContent = messageContent;
//		this.senderUserNo = senderUserNo;
		this.senderUserNickName = senderUserNickName;
//		this.receiverUserNo = receiverUserNo;
//		this.receiverNickName = receiverNickName;
	}

	
}
