package com.mli.websocket.websocket.test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.util.HtmlUtils;

import com.mli.websocket.manager.model.Manager;
import com.mli.websocket.manager.service.ManagerService;
import com.mli.websocket.user.model.UserInfo;
import com.mli.websocket.user.service.UserService;
import com.mli.websocket.websocket.config.WebSocketEventListener;
import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.model.ClientMessage.ReceiverRole;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;
import com.mli.websocket.websocket.model.ServerMessage;
import com.mli.websocket.websocket.service.ClientMessageService;
import com.mli.websocket.websocket.service.ExportExcelService;
import com.mli.websocket.websocket.test.ClientMessageDto.MessageType;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("websocket")
//@CrossOrigin(origins = "*") 跟他沒關係XD
public class TestWebSocketController {

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate; // Provides methods for sending messages to a user.

	@Autowired
	private UserService userService;

	@Autowired
	private ClientMessageService clientMessageService;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private ExportExcelService exportExcelService;

	private final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	// 存訊息 + 查訊息 + 刪除訊息 直接用websocket存取

//	這區是可以成功多人聊天, 已取Session把User資料丟給Websocket, 已連接資料庫新增訊息, 尚未做輸出歷史訊息. 

//	/**
//	 * Enter into main page of chat room Get and save httpsession
//	 */
//	@GetMapping("/publicChatRoom")
//	public String chatRoomPage(HttpSession httpSession, Model model) {
//
//		Integer userNo = (Integer) httpSession.getAttribute("userNo");
//		Integer managerId = (Integer) httpSession.getAttribute("managerId");
//
//		if (userNo != null) {
//			UserInfo userInfo = userService.findUserByNo(userNo);
//			String userNickname = userInfo.getUserNickname();
//
//			httpSession.setAttribute("userNickname", userNickname);
//			httpSession.setAttribute("userNo", userNo);
//			httpSession.setAttribute("senderRole", SenderRole.USER);
//
//			model.addAttribute("userNo", userNo);
//			model.addAttribute("userNickname", userNickname);
//
//		} else if (managerId != null) {
//			Manager manager = managerService.findManagerById(managerId);
//			String managerName = manager.getManagerName();
//
//			httpSession.setAttribute("managerName", managerName);
//			httpSession.setAttribute("managerId", managerId);
//			httpSession.setAttribute("senderRole", SenderRole.MANAGER);
//
//			model.addAttribute("managerId", managerId);
//			model.addAttribute("managerName", managerName);
//		}
//		return "websocket/testpublicChatRoom2";
//	}
//
//	@MessageMapping("/messageAddUser")
//	@SendTo("/chatroom/public")
//	public ClientMessage addUser(@Payload ClientMessage clientMessage, SimpMessageHeaderAccessor headerAccessor) {
//
//		Integer userNo = (Integer) headerAccessor.getSessionAttributes().get("userNo");
//		Integer managerId = (Integer) headerAccessor.getSessionAttributes().get("managerId");
//
//		ClientMessage newClientMessage = new ClientMessage();
//		newClientMessage.setMessageType(clientMessage.getMessageType());
//		newClientMessage.setMessageDatetime(LocalDateTime.now());
//		newClientMessage.setMessageContent(clientMessage.getMessageContent());
//		newClientMessage.setReceiverRole(ReceiverRole.ALL); // All
//
//		if (userNo != null) {
//			UserInfo userInfo = userService.findUserByNo(userNo);
//			String senderUserNickName = userInfo.getUserNickname();
//
//			if (senderUserNickName != null) {
//				headerAccessor.getSessionAttributes().put("username", senderUserNickName);
//				headerAccessor.getSessionAttributes().put("userNo", userNo);
//				logger.info("username" + senderUserNickName);
//				logger.info("userNo" + userNo);
//			} else {
//				logger.info("username is null");
//			}
//
//			newClientMessage.setSenderRole(SenderRole.USER); // USER
//			newClientMessage.setSenderUserNo(userNo);
//			newClientMessage.setSenderUserNickName(senderUserNickName);
//
//			clientMessageService.insertClientMessage(newClientMessage);
//
//			SenderRole senderRole = newClientMessage.getSenderRole();
//			Integer senderUserNo = newClientMessage.getSenderUserNo();
//
//			headerAccessor.getSessionAttributes().put("senderRole", senderRole);
//			headerAccessor.getSessionAttributes().put("senderUserNo", senderUserNo);
//
//			return newClientMessage;
//
//		} else if (managerId != null) {
//			Manager manager = managerService.findManagerById(managerId);
//			String senderUserNickName = manager.getManagerName();
//
//			if (senderUserNickName != null) {
//				headerAccessor.getSessionAttributes().put("username", senderUserNickName);
//				headerAccessor.getSessionAttributes().put("managerId", managerId);
//				logger.info("username" + senderUserNickName);
//				logger.info("managerId" + managerId);
//			} else {
//				logger.info("username is null");
//			}
//
//			newClientMessage.setSenderRole(SenderRole.MANAGER);
//			newClientMessage.setSenderManagerId(managerId);
//			newClientMessage.setSenderUserNickName(senderUserNickName);
//
//			clientMessageService.insertClientMessage(newClientMessage);
//
//			SenderRole senderRole = newClientMessage.getSenderRole();
//			Integer senderManagerId = newClientMessage.getSenderManagerId();
//
//			headerAccessor.getSessionAttributes().put("senderRole", senderRole);
//			headerAccessor.getSessionAttributes().put("senderManagerId", senderManagerId);
//			return newClientMessage;
//		}
//		return newClientMessage;
//	}
//
//	/**
//	 * Public Chat room
//	 * 
//	 */
//	@MessageMapping("/messageSendMessage") // /app/messageSendMessage
//	@SendTo("/chatroom/public")
//	public ClientMessage receiveClientMessage(@Payload ClientMessage clientMessage,
//			SimpMessageHeaderAccessor headerAccessor) {
//
//		SenderRole senderRole = (SenderRole) headerAccessor.getSessionAttributes().get("senderRole");
//		
//		if (senderRole.equals(SenderRole.USER)) {
//			Integer senderUserNo = (Integer) headerAccessor.getSessionAttributes().get("senderUserNo");
//
//			UserInfo userInfo = userService.findUserByNo(senderUserNo);
//			String senderUserNickName = userInfo.getUserNickname();
//
//			ClientMessage newClientMessage = new ClientMessage();
//			newClientMessage.setMessageType(clientMessage.getMessageType());
//			newClientMessage.setMessageDatetime(LocalDateTime.now());
//			newClientMessage.setMessageContent(clientMessage.getMessageContent());
//
//			newClientMessage.setSenderRole(SenderRole.USER);
//			newClientMessage.setSenderUserNo(senderUserNo);
//			newClientMessage.setSenderManagerId(clientMessage.getSenderManagerId());
//			newClientMessage.setSenderUserNickName(senderUserNickName);
//
//			newClientMessage.setReceiverRole(ReceiverRole.ALL);
//
//			clientMessageService.insertClientMessage(newClientMessage);
//
//			return newClientMessage;
//
//		} else if (senderRole.equals(SenderRole.MANAGER)) {
//			Integer senderManagerId = (Integer) headerAccessor.getSessionAttributes().get("senderManagerId");
//			
//			Manager manager = managerService.findManagerById(senderManagerId);
//			String senderUserNickName = manager.getManagerName();
//			
//			ClientMessage newClientMessage = new ClientMessage();
//			newClientMessage.setMessageType(clientMessage.getMessageType());
//			newClientMessage.setMessageDatetime(LocalDateTime.now());
//			newClientMessage.setMessageContent(clientMessage.getMessageContent());
//
//			newClientMessage.setSenderRole(SenderRole.MANAGER);
//			newClientMessage.setSenderManagerId(senderManagerId);
//			newClientMessage.setSenderUserNickName(senderUserNickName);
//
//			newClientMessage.setReceiverRole(ReceiverRole.ALL);
//
//			clientMessageService.insertClientMessage(newClientMessage);
//
//			return newClientMessage;
//		}
//		return null;
//	}

//	@MessageMapping("/messageAddUser")
//	@SendTo("/chatroom/public")
//	public ClientMessage addUser(@Payload ClientMessage clientMessage, SimpMessageHeaderAccessor headerAccessor) {
//
//		Integer userNo = (Integer) headerAccessor.getSessionAttributes().get("userNo");
//		UserInfo userInfo = userService.findUserByNo(userNo);
//		String senderUserNickName = userInfo.getUserNickname();
//
//		if (senderUserNickName != null) {
//			headerAccessor.getSessionAttributes().put("username", senderUserNickName);
//			headerAccessor.getSessionAttributes().put("userNo", userNo);
//			logger.info("username" + senderUserNickName);
//			logger.info("userNo" + userNo);
//		} else {
//			logger.info("username is null");
//		}
//
//		ClientMessage newClientMessage = new ClientMessage();
//		newClientMessage.setMessageType(clientMessage.getMessageType());
//		newClientMessage.setMessageDatetime(LocalDateTime.now());
//		newClientMessage.setMessageContent(clientMessage.getMessageContent());
//		newClientMessage.setSenderRole(SenderRole.USER); // USER
//		newClientMessage.setSenderUserNo(userNo);
//		newClientMessage.setSenderUserNickName(senderUserNickName);
//		newClientMessage.setReceiverRole(ReceiverRole.ALL); // All
//		clientMessageService.insertClientMessage(newClientMessage);
//
//		SenderRole senderRole = newClientMessage.getSenderRole();
//		Integer senderUserNo = newClientMessage.getSenderUserNo();
//
//		headerAccessor.getSessionAttributes().put("senderRole", senderRole);
//		headerAccessor.getSessionAttributes().put("senderUserNo", senderUserNo);
//
//		return newClientMessage;
//	}
//
//	@MessageMapping("/messageAddManager")
//	@SendTo("/chatroom/public")
//	public ClientMessage addManager(@Payload ClientMessage clientMessage, SimpMessageHeaderAccessor headerAccessor) {
//
//		Integer managerId = (Integer) headerAccessor.getSessionAttributes().get("managerId");
//		
//	        	Manager manager = managerService.findManagerById(managerId);
//				String senderUserNickName = manager.getManagerName();
//				
//				if (managerId != null) {	
//				ClientMessage newClientMessage = new ClientMessage();
//				newClientMessage.setMessageType(clientMessage.getMessageType());
//				newClientMessage.setMessageDatetime(LocalDateTime.now());
//				newClientMessage.setMessageContent(clientMessage.getMessageContent());
//				newClientMessage.setSenderRole(SenderRole.MANAGER); // USER
//				newClientMessage.setSenderManagerId(managerId);
//				newClientMessage.setSenderUserNickName(senderUserNickName);
//				newClientMessage.setReceiverRole(ReceiverRole.ALL); // All
//				clientMessageService.insertClientMessage(newClientMessage);
//
//				SenderRole senderRole = newClientMessage.getSenderRole();
//				Integer senderMangerId = newClientMessage.getSenderManagerId();
//
//				headerAccessor.getSessionAttributes().put("senderRole", senderRole);
//				headerAccessor.getSessionAttributes().put("senderMangerId", senderMangerId);
//				return newClientMessage;
//	    } else {
//	        logger.info("Both userNo and managerId are null");
//	        return null;
//	    }
//	}

//	@ResponseBody
//	@GetMapping("listChattingHistoryByUserNo")
//	public List<ClientMessage> listChattingHistoryByUserNo(@RequestParam("senderUserNo") Integer senderUserNo,
//			HttpSession httpSession, Model model) {
//
////		Integer sessionUserNo = (Integer)httpSession.getAttribute("userNo");
////		UserInfo userInfo = userService.findUserByNo(sessionUserNo);
////		Integer userNo = userInfo.getUserNo();
////		System.out.println("Session UserNo: " + sessionUserNo);
////		System.out.println("userInfo UserNo: " + userInfo);
////		System.out.println("userNo UserNo: " + userNo);
//		List<ClientMessage> listMessageFromOneUser = clientMessageService.listMessageFromOneUser(senderUserNo);
//		model.addAttribute("listMessageFromOneUser", listMessageFromOneUser);
//		return listMessageFromOneUser;
////		return "websocket/testListChatHistory";
//	}
//
//	@GetMapping("/exportChattingHistroy")
//	public void exportChattingHistroy(HttpServletResponse response, HttpSession httpSession) throws IOException {
//
//		List<ClientMessage> clientMessageList = (List<ClientMessage>) httpSession.getAttribute("listAllClientMessage");
//		XSSFWorkbook exportChattingHistoryToExcel = exportExcelService.exportChattingHistoryToExcel(clientMessageList);
//
//		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//		response.setHeader("Content-Disposition", "attachment; filename=Chatting_History.xlsx");
//		exportChattingHistoryToExcel.write(response.getOutputStream());
//		exportChattingHistoryToExcel.close();
//	}
//
//	@GetMapping("/listAllChattingMessages")
//	public String listAllChattingMessages(HttpSession httpSession, Model model) {
//
//		List<ClientMessage> listAllClientMessage = clientMessageService.listAllClientMessage();
//		model.addAttribute("listAllClientMessage", listAllClientMessage);
//		httpSession.setAttribute("listAllClientMessage", listAllClientMessage);
//		return "websocket/testListChatHistory";
//	}

//	@GetMapping("exportChattingHistory")
//	public String exportChattingHistory(HttpSession httpSession) {
//		
//		return ;
//	}

//	@GetMapping("/testChatRoom")
//	public String chatRoomPage(Model model, HttpSession httpSession) {
////		String userNickname = (String)httpSession.getAttribute("userNickname");
////		Integer userNo = (Integer)httpSession.getAttribute("userNo");
////		UserInfo userInfo = userService.findUserByNo(userNo);
////		model.addAttribute("userInfo", userInfo);
////		model.addAttribute("userNickname", userNickname);
//
//		return "websocket/testPublicChatRoom";
//	}

//	 想辦法連結資料庫&取得Session

//	@GetMapping("/testPublicChatRoom")
//	public String publicChatRoomPage(HttpSession httpSession, Model model) {
//		Integer userNo = (Integer)httpSession.getAttribute("userNo"); 
//		UserInfo userInfo = userService.findUserByNo(userNo);
//		String userNickname = userInfo.getUserNickname();
//		
//		httpSession.setAttribute("userNickname", userNickname);
//		httpSession.setAttribute("userNo", userNo);
//		
//		model.addAttribute("userNo", userNo);
//		model.addAttribute("userNickname", userNickname);
//		return "websocket/testPublicChatRoom";
//	}
//	
//	@MessageMapping("/message.sendMessage") // /app/message
//	@SendTo("/chatroom/public")
//	public ClientMessageDto receiveClientMessageDto(@Payload ClientMessageDto clientMessageDto) {
//		return clientMessageDto;
//	}
//
//	@MessageMapping("/message.addUser")
//	@SendTo("/chatroom/public")
//	public ClientMessageDto addUser(@Payload ClientMessageDto clientMessageDto,
//			SimpMessageHeaderAccessor headerAccessor) {
//		
//		Integer userNo = (Integer)headerAccessor.getSessionAttributes().get("userNo");
//		String senderUserNickName = (String)headerAccessor.getSessionAttributes().get("userNickname");
//		
//		if(senderUserNickName!=null) {
//			headerAccessor.getSessionAttributes().put("username", senderUserNickName);
//			headerAccessor.getSessionAttributes().put("userNo", userNo);
//			logger.info("username" + senderUserNickName);
//			logger.info("userNo" + userNo);
//		}else {
//			logger.info("username is null");
//		}
//		return clientMessageDto;
//	}
	// ------------------------------------------------------------------------------

	/**
	 * Private Chat room
	 */
	@MessageMapping("/privateMessage")
	public ClientMessageDto receivePrivateClientMessage(@Payload ClientMessageDto clientMessageDto,
			HttpSession session) {
		Integer userNo = (Integer) session.getAttribute("userNo");
//		clientMessageDto.setSenderUserNo(userNo);
		UserInfo userInfo = userService.findUserByNo(userNo);
		String userNickname = userInfo.getUserNickname();

		simpMessagingTemplate.convertAndSendToUser(userNickname, "/private", clientMessageDto); // user/Nickname/private
		return clientMessageDto;

	}
//	---------------------------------------------------------------------------------
	// Get session - which one is better??
//	
//	@MessageMapping("/message")
//	@SendTo("/chatroom/public")
//	public ClientMessageDto addReceiveClientMessageDto(@Payload ClientMessageDto clientMessageDto, SimpMessageHeaderAccessor headerAccessor) {
//	    
//		headerAccessor.getSessionAttributes().put("userNickname", clientMessageDto.getSenderUserNickName());
//		
////	    String userNickname = (String) headerAccessor.getSessionAttributes().get("userNickname");
////	    Integer userNo = (Integer) headerAccessor.getSessionAttributes().get("userNo");
////	    
////	    clientMessageDto.setSenderUserNickName(userNickname);
////	    clientMessageDto.setSenderUserNo(userNo);
//	    
//	    return clientMessageDto;
//	}
//	

	@GetMapping("/getUserDetailsByHttpSession")
	public ResponseEntity<Map<String, Object>> getUserDetails(HttpSession session) {
		Map<String, Object> userDetails = new HashMap<>();
		userDetails.put("userNo", session.getAttribute("userNo"));
		userDetails.put("userNickname", session.getAttribute("userNickname"));
		return ResponseEntity.ok(userDetails);
	}
	
	
	
	/**
	 * find messages by userNo (尚未用到)
	 * */
	@ResponseBody
	@GetMapping("listChattingHistoryByUserNo")
	public List<ClientMessage> listChattingHistoryByUserNo(@RequestParam("senderUserNo") Integer senderUserNo,
			HttpSession httpSession, Model model) {

//		Integer sessionUserNo = (Integer)httpSession.getAttribute("userNo");
//		UserInfo userInfo = userService.findUserByNo(sessionUserNo);
//		Integer userNo = userInfo.getUserNo();
//		System.out.println("Session UserNo: " + sessionUserNo);
//		System.out.println("userInfo UserNo: " + userInfo);
//		System.out.println("userNo UserNo: " + userNo);
		List<ClientMessage> listMessageFromOneUser = clientMessageService.listMessageFromOneUser(senderUserNo);
		model.addAttribute("listMessageFromOneUser", listMessageFromOneUser);
		return listMessageFromOneUser;
//		return "websocket/testListChatHistory";
	}


}
