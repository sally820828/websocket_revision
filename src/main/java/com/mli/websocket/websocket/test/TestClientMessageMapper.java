package com.mli.websocket.websocket.test;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.model.ClientMessage.ReceiverRole;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;
/**
 * Mapper Interface for `client_message`
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.06
 */
@Mapper
public interface TestClientMessageMapper {
	
	/**
	 * Insert messages to DB
	 * @return ClientMessage
	 */
	int insertClientMessage(ClientMessage clientMessage);
	
	/**
	 * List All Client Messages
	 * @return List<ClientMessage>
	 */
	List<ClientMessage> listAllClientMessage();
	
	//查詢分Public / Private 再分 User / Manager 較複雜的查詢之後再補
	/**
	 * List All Messages By ReceiverRole (Public or Private) 
	 * @return List<ClientMessage>
	 */
	List<ClientMessage> listMessageByReceiverRole(ReceiverRole receiverRole);
	
	/**
	 * List All Messages By SenderRole (User or Manager)
	 * @return List<ClientMessage>
	 */
	List<ClientMessage> listMessageBySenderRole(SenderRole senderRole);
	
	/**
	 * List All Messages from one User
	 * @return List<ClientMessage>
	 */
	List<ClientMessage> listMessageFromOneUser(Integer senderUserNo);
	
	/**
	 * List All Messages from one Manager
	 * @return List<ClientMessage>
	 */
	List<ClientMessage> listMessageFromOneManager(Integer senderManagerId);
	
	/**
	 * Delete one message 
	 * @return boolean
	 */
	boolean deleteClientMessage(Integer messageId);
}
