package com.mli.websocket.websocket.test;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class MyWebSocketHandler extends TextWebSocketHandler{

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		
	     String userNickname = (String) session.getAttributes().get("userNickname");
	        
	     session.sendMessage(new TextMessage("Hello, " + userNickname + "! You sent: " + message.getPayload()));
	}

}
