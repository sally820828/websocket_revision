package com.mli.websocket.websocket.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.websocket.websocket.mapper.ClientMessageMapper;
import com.mli.websocket.websocket.model.ClientMessage;
import com.mli.websocket.websocket.model.ClientMessage.ReceiverRole;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;

/**
 * Service for manage table `client_message` in SQL SERVER
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.06
 */
@Service
public class ClientMessageService {
	
	@Autowired
	private ClientMessageMapper clientMessageMapper;
	
	/**
	 * Insert message to DB
	 * @return ClientMessage
	 */
	public int insertClientMessage(ClientMessage clientMessage) {
		try {
			if (clientMessage == null) {
				return -1;
			}
			int insertClientMessage = clientMessageMapper.insertClientMessage(clientMessage);
			return insertClientMessage;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * List All Client Messages
	 * @return List<ClientMessage>
	 */
	public List<ClientMessage> listAllClientMessage() {
		List<ClientMessage> allClientMessageList = clientMessageMapper.listAllClientMessage();
		if (allClientMessageList != null) {
			return allClientMessageList;
		} else {
			return null;
		}
	}

	
	//以下尚未使用//
	/**
	 * List All Messages By ReceiverRole (Public or Private) 
	 * @return List<ClientMessage>
	 */
	public List<ClientMessage> listMessageByReceiverRole(ReceiverRole receiverRole) {

		List<ClientMessage> listMessageByReceiverRole = clientMessageMapper.listMessageByReceiverRole(receiverRole);
		if (listMessageByReceiverRole != null) {
			return listMessageByReceiverRole;
		} else {
			return null;
		}
	}
	
	/**
	 * List All Messages By SenderRole (User or Manager)
	 * @return List<ClientMessage>
	 */
	public List<ClientMessage> listMessageBySenderRole(SenderRole senderRole) {

		List<ClientMessage> listMessageBySenderRole = clientMessageMapper.listMessageBySenderRole(senderRole);
		if (listMessageBySenderRole != null) {
			return listMessageBySenderRole;
		} else {
			return null;
		}
	}
	/**
	 * List All Messages from one User
	 * @return List<ClientMessage>
	 */
	public List<ClientMessage> listMessageFromOneUser(Integer senderUserNo) {

		List<ClientMessage> listMessageFromOneUser = clientMessageMapper.listMessageFromOneUser(senderUserNo);
		if (listMessageFromOneUser != null) {
			return listMessageFromOneUser;
		} else {
			return null;
		}
	}
	/**
	 * List All Messages from one Manager
	 * @return List<ClientMessage>
	 */
	public List<ClientMessage> listMessageFromOneManager(Integer senderManagerId) {

		List<ClientMessage> listMessageFromOneManager = clientMessageMapper.listMessageFromOneManager(senderManagerId);
		if (listMessageFromOneManager != null) {
			return listMessageFromOneManager;
		} else {
			return null;
		}
	}

	/**
	 * Delete one message 
	 * 
	 * @return boolean
	 */
	public boolean deleteClientMessage(Integer messageId) {
		boolean deleteClientMessage = clientMessageMapper.deleteClientMessage(messageId);
		return deleteClientMessage;
	}

}
