package com.mli.websocket.websocket.service;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.mli.websocket.websocket.model.ClientMessage;

@Service
public class ExportExcelService {

	public XSSFWorkbook exportChattingHistoryToExcel(List<ClientMessage> clientMessageList) {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Chatting History");

		XSSFRow xssfRow = sheet.createRow(0);
		xssfRow.createCell(0).setCellValue("messageId");
		xssfRow.createCell(1).setCellValue("messageType");
		xssfRow.createCell(2).setCellValue("messageDatetime");
		xssfRow.createCell(3).setCellValue("messageContent");
		xssfRow.createCell(4).setCellValue("senderUserRole");
		xssfRow.createCell(5).setCellValue("senderUserNo");
		xssfRow.createCell(6).setCellValue("senderManagerId");
		xssfRow.createCell(7).setCellValue("receiverRole");
		xssfRow.createCell(8).setCellValue("receiverUserNo");
		xssfRow.createCell(9).setCellValue("receiverManagerId");

		int rowNum = 1;
		for (ClientMessage clientMessage : clientMessageList) {
			XSSFRow dataRow = sheet.createRow(rowNum++);
			dataRow.createCell(0).setCellValue(clientMessage.getMessageId());
			dataRow.createCell(1).setCellValue(clientMessage.getMessageType().toString());
			dataRow.createCell(2).setCellValue(clientMessage.getMessageDatetime().toString());
			dataRow.createCell(3).setCellValue(clientMessage.getMessageContent());
			dataRow.createCell(4).setCellValue(clientMessage.getSenderRole().toString());
			
			if (clientMessage.getSenderUserNo() == null) {
				dataRow.createCell(5).setBlank();
			} else {
				dataRow.createCell(5).setCellValue(clientMessage.getSenderUserNo());
			}
			
			if (clientMessage.getSenderManagerId() == null) {
				dataRow.createCell(6).setBlank();
			} else {
				dataRow.createCell(6).setCellValue(clientMessage.getSenderManagerId());
			}
			
			dataRow.createCell(7).setCellValue(clientMessage.getReceiverRole().toString());
			
			if (clientMessage.getReceiverUserNo() == null) {
				dataRow.createCell(8).setBlank();
			} else {
				dataRow.createCell(8).setCellValue(clientMessage.getReceiverUserNo());
			}
			
			if (clientMessage.getReceiverManagerId() == null) {
				dataRow.createCell(9).setBlank();
			} else {
				dataRow.createCell(9).setCellValue(clientMessage.getReceiverManagerId());
			}
		}

		XSSFRow sumRow = sheet.createRow(rowNum);
		sumRow.createCell(0).setCellValue("Total: ");

		return workbook;
	}
}

//			dataRow.createCell(6).setCellValue(clientMessage.getSenderUserNo());
//			dataRow.createCell(7).setCellValue(clientMessage.getSenderManagerId());
//			dataRow.createCell(9).setCellValue(clientMessage.getReceiverUserNo());
//			dataRow.createCell(10).setCellValue(clientMessage.getReceiverManagerId());