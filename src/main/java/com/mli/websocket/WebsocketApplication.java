package com.mli.websocket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.mli.websocket.user.mapper")
@MapperScan("com.mli.websocket.manager.mapper")
@MapperScan("com.mli.websocket.websocket.mapper")
public class WebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketApplication.class, args);
	}

}
