package com.mli.websocket.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.LocalDateTimeTypeHandler;
import org.apache.ibatis.type.MappedJdbcTypes;

/**
 * LocalDatetimeHandler for MyBatis
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.09
 */
@MappedJdbcTypes(JdbcType.DATE)
public class LocalDatetimeHandler extends LocalDateTimeTypeHandler {
	
	/**
	 * 將 LocalDateTime 物件轉換為毫秒數
	 * atZone(轉換為系統預設時區.toInstant().toEpochMilli轉換為毫秒數)
	 */
	private long toTimeMillis(LocalDateTime localDateTime) {
		return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}
	
	/**
	 * 將毫秒數轉換回LocalDateTime 物件
	 * 
	 */
	private LocalDateTime toLocalDateTime(long timeMillis) {
		return new Date(timeMillis).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
	
	/**
	 * 將java類型轉換成資料庫需要的類型
	 */
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, LocalDateTime localDateTime, JdbcType jdbcType)
			throws SQLException {
		Timestamp timestamp = new Timestamp(this.toTimeMillis(localDateTime));
		preparedStatement.setTimestamp(i, timestamp);
	}

	/**
	 *將資料庫中類型轉換成java類型
	 */
	@Override
	public LocalDateTime getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
	    Timestamp timestamp = resultSet.getTimestamp(columnName);
	    if (timestamp != null) {
	        return this.toLocalDateTime(timestamp.getTime());
	    }
	    return null;
	}
	
	/**
	 *將資料庫類型轉換成java類型(ResultSet)
	 */
	@Override
	public LocalDateTime getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
		Timestamp timestamp = resultSet.getTimestamp(columnIndex);
		if (timestamp!=null) {
			return this.toLocalDateTime(timestamp.getTime());
		}
		return null;
	}

	/**
	 *將資料庫中類型轉換成java類型(CallableStatement)
	 */
	@Override
	public LocalDateTime getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		Timestamp timestamp = callableStatement.getTimestamp(columnIndex);
		if(timestamp!=null) {
			return this.toLocalDateTime(timestamp.getTime());
		}
		return null;
	}

	public LocalDatetimeHandler() {
	}
}