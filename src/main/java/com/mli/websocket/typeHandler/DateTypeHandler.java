package com.mli.websocket.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

/**
 * DateTypeHandler for 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.09.21
 */
@MappedJdbcTypes(JdbcType.DATE)
public class DateTypeHandler extends BaseTypeHandler<Date> {

	/**
	 *將java類型轉換成資料庫需要的類型
	 */
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, Date date, JdbcType jdbcType)
			throws SQLException {
		long time = date.getTime();
		preparedStatement.setLong(i, time);
	}

	/**
	 *將資料庫中類型轉換成java類型
	 */
	@Override
	public Date getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
	    Timestamp timestamp = resultSet.getTimestamp(columnName);
	    if (timestamp != null) {
	        return new Date(timestamp.getTime());
	    }
	    return null;
	}
	/**
	 *將資料庫類型轉換成java類型
	 */
	@Override
	public Date getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
		long aLong = resultSet.getLong(columnIndex);
		Date date = new Date(aLong);
		return date;
	}

	/**
	 *將資料庫中類型轉換成java類型
	 */
	@Override
	public Date getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		long aLong = callableStatement.getLong(columnIndex);
		Date date = new Date(aLong);
		return date;
	}

	public DateTypeHandler() {
		super();
	}
	
	
}