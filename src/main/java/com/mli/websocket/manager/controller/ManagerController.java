package com.mli.websocket.manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mli.websocket.config.RandomStringGenerator;
import com.mli.websocket.manager.model.Manager;
import com.mli.websocket.manager.service.ManagerService;
import com.mli.websocket.user.service.MailService;
import com.mli.websocket.websocket.model.ClientMessage.SenderRole;

import jakarta.servlet.http.HttpSession;

/**
 * Manage database through the controller 
 * @author D2082803 Sally 
 * @version 1.0
 * @since 2023.09.21
 */
@RequestMapping("manager")
@Controller
public class ManagerController {

	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private MailService mailService;
	
//	private Logger logger = LoggerFactory.getLogger(UserController.class);
	
//	登入登出專區================================================================================================================
	/** 
	 * Enter Login Page
	 */
	@GetMapping("/managerloginPage")
	public String loginPage(HttpSession session, Model model) {
		String captcha = RandomStringGenerator.getRandomStringByLength(4);
		session.setAttribute("captcha", captcha);
		model.addAttribute("firstCaptcha",captcha);
		return "manager/managerLoginPage";
	}
	
	/**
	 * Change captcha 亂數驗證
	 */
	@ResponseBody
	@GetMapping("/changeCapcha")
	public String changeCapcha(HttpSession session) {
		String captcha = RandomStringGenerator.getRandomStringByLength(4);
		session.setAttribute("captcha", captcha);
		return captcha;		
	}
	
	/**
	 * Login - Check the managerId & managerPassword with database
	 */
	@PostMapping("/managerLoginProcess")
	public String loginProcess(@RequestParam("managerEmail") String managerEmail, 
							   @RequestParam("managerPassword") String managerPassword, 
							   @RequestParam("captcha") String insertCaptcha,
							   HttpSession session,
							   Model model) {
		Manager manager = managerService.findManagerByEmailPassword(managerEmail, managerPassword);
		String captcha = (String)session.getAttribute("captcha");
		
		if(manager != null && captcha.equals(insertCaptcha)) {
			        session.setAttribute("manager", manager);
					model.addAttribute("manager",manager);
					
					Integer managerId = manager.getManagerId();
					session.setAttribute("managerId", managerId);
					String managerName = manager.getManagerName();
					session.setAttribute("managerName", managerName); // For聊天室用
					session.setAttribute("senderRole", SenderRole.MANAGER);
					
					return "manager/managerLoginSuccess";
		} else {
			return "redirect:/manager/loginPage?error=true";
		}
	}
	
//	/** 
//	 * Signup Page 
//	 * 
//	 * */
//	@GetMapping("/signupPage")
//	public String signupPage() {
//		return "manager/signupPage";
//	}
//	
//	/**
//	 * Sign up 
//	 */
//	@PostMapping("/signupProcess")
//	public String signupProcess(@ModelAttribute("manager") Manager manager, Model model, HttpSession session)	{
//		Manager insertUser = managerService.insertManager(manager);
//		session.setAttribute("insertUser", insertUser);
//		return "redirect:/manager/loginPage"; 
//	}
//	
//	/**
//	 * Forgot Password Page
//	 * */
//	@GetMapping("/forgetPasswordPage")
//	public String forgetPasswordPage(HttpSession session, Model model) {
//		String captcha = RandomStringGenerator.getRandomStringByLength(4);
//		session.setAttribute("captcha", captcha);
//		model.addAttribute("firstCaptcha",captcha);
//		return "manager/forgetPassword";
//	}
//	
//	/**
//	 * Forget Password -> send mail
//	 * */
//	@PostMapping("/javamail")
//	public String sendEmail(@RequestParam("managerId") Integer managerId, @RequestParam("captcha") String insertCaptcha, HttpSession session, Model model) {
//		
//		String captcha = (String)session.getAttribute("captcha");
//		
//		if(!captcha.equals(insertCaptcha)) {
//			return "redirect:/manager/loginPage?error=true";
//		}
//		
//		String managerPassword = managerService.findManagerById(managerId).getManagerPassword();
//		
//		String managerEmail = managerService.findManagerById(managerId).getManagerEmail();
//		String title = "Forgot Password";
//		String message = "Your Password is " + managerPassword;
//		mailService.sendEmail(managerEmail, title, message);
//		
//		return "manager/forgetPassword";
//	}
//	
////	User資料專區================================================================================================================	
//	
//	/**
//	 * Read all list
//	 * @return List<Manager>
//	 */
////	@Operation(summary = "Read all data", description = "Read all data from table `Manager`")
//	@GetMapping("/listAllManager")
//	public String listAllManager(HttpSession session, Model model) {
////		try {
//			List<Manager> ManagerList = managerService.findAllManager();
//			model.addAttribute("manager", ManagerList);
////			if(ManagerList != null) {
////				logger.debug("Successful!");
//				return "manager/managerAlllist";
////			}else{
////				logger.debug("There is no files.");
////				return HttpStatus.NOT_FOUND;
////			}
////		} catch (Exception e) {
////			logger.error("Error! Failed to read table");
////			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
////		}
//	}
////	
//	/**
//	 * Read one row
//	 * @return Manager
//	 */
////	@Operation(summary = "Read one row", description = "Read one row by managerId")
//	@GetMapping("/findOneManagerByNo")
//	public String findOneManager(@RequestParam("managerId")Integer managerId, HttpSession session, Model model ){
////		try {
//			Manager findOneManager = managerService.findManagerById(managerId);
////			if(findOneManager != null) {
//				model.addAttribute("manager", findOneManager);
////				logger.debug("Read list successfully!");
//				return "manager/managerEdit";
////			}else {
////				logger.debug("Manager Not Found");
////				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
////			}
////		} catch (Exception e) {
////			logger.error("Error");
////			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
////		}
////			return null;
//	}
////	
//	/**
//	 * Edit one row
//	 * @return Manager
//	 */
////	@Operation(summary = "Edit one row", description = "Edit one row by managerId")
//	@PutMapping("/updateManager")
//	public String updateManager(@RequestBody Manager manager, HttpSession session, Model model){
////		try {
//			Manager updateManager = managerService.updateManager(manager);
//			model.addAttribute("manager",updateManager);
//			return "redirect:/manager/managerAlllist";
////			if(updateManager != null) {
////				logger.debug("Successful!");
////				return ResponseEntity.ok(updateManager);
////			}else {
////				logger.debug("the row is not exist");
////				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
////			}
////		} catch (Exception e) {
////			logger.error("Error");
////			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
////		}
//	}
//	
//	/**
//	 * Delete one row
//	 * @return Manager
//	 */
////	@Operation(summary = "Delete one row", description = "Delete one row by managerId")
//	@DeleteMapping("/deleteManager")
//	public String deleteManager(@RequestParam("managerId")Integer managerId, HttpSession session ){
////		try {
//			managerService.deleteManager(managerId);
//			return "redirect:/manager/listAllManager";
////			if(deleteManager) {
////				logger.debug("Deleted successfully!");
////				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
////			}else {
////				logger.debug("the row is not exist");
////				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
////			}
////		} catch (Exception e) {
////			logger.error("Error");
////			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
////		}
//	}
	
}
