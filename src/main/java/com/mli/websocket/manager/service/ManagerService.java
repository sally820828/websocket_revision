package com.mli.websocket.manager.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.core.userdetails.ManagerDetails;
//import org.springframework.security.core.userdetails.ManagerDetailsService;
//import org.springframework.security.core.userdetails.ManagernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mli.websocket.manager.mapper.ManagerMapper;
import com.mli.websocket.manager.model.Manager;

/**
 * Service for manage table `manager` in SQLite
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.09.21
 */
@Service
public class ManagerService {
	
	@Autowired
	private ManagerMapper managerMapper;
	

	private Logger logger = LoggerFactory.getLogger(ManagerService.class);

	/**
	 * 信箱密碼登入
	 * */
	public Manager findManagerByEmailPassword(String managerEmail, String managerPassword) {
		if (managerEmail == null) {
			logger.error("managerEmail is null");
			return null;
		}
		if (managerPassword == null) {
			logger.error("managerPassword is null");
			return null;
		}
		try {
			Manager queryManager = managerMapper.findManagerByEmailPassword(managerEmail, managerPassword);
			if (queryManager == null) {
				logger.warn("Manager doesn't exist for given email and password");
			}
			logger.info("Manager exists!");
			return queryManager;
		} catch (Exception e) {
			logger.error("Error occurred while selecting manger by email & password: " + e.getMessage(), e);
		}
		return null;
	}
	
	
//	--------------------------------------------
	/**
	 * Insert one row
	 * @return Manager
	 */
	public Manager insertManager(Manager manager) {
		try {
			if (manager == null) {
				return null;
			}
//			String encodePassword = passwordEncoder.encode(Manager.getManagerPassword());
//			Manager.setManagerPassword(encodePassword);
			Manager insertManager = managerMapper.insertManager(manager);
			return insertManager;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manager;
	}

	/**
	 * Get all list
	 * @return List<Manager>
	 */
	public List<Manager> findAllManager() {
		List<Manager> allManagerList = managerMapper.findAllManager();
		if (allManagerList != null) {
			return allManagerList;
		} else {
			return null;
		}
	}

	/**
	 * Get one row
	 * 
	 * @return Manager
	 */
	public Manager findManagerById(Integer managerId) {
		Manager queryManager = managerMapper.findManagerById(managerId);
		if (queryManager != null) {
			return queryManager;
		} else {
			return null;
		}
	}

	/**
	 * 帳號密碼登入
	 * */
	public Manager findManagerByIdPassword(Integer managerId, String managerPassword) {
	    Manager queryManager = managerMapper.findManagerByIdPassword(managerId, managerPassword);
	    return queryManager;
	}

	/**
	 * Edit one row
	 * 
	 * @return Manager
	 */
	public Manager updateManager(Manager manager) {
		Manager updateManager = managerMapper.updateManager(manager);
		if (updateManager != null) {
			return manager;
		} else {
			return null;
		}
	}

	/**
	 * Delete one row
	 * 
	 * @return boolean
	 */
	public boolean deleteManager(Integer managerId) {
		boolean deleteManager = managerMapper.deleteManager(managerId);
		return deleteManager;
	}

}
