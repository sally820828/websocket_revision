package com.mli.websocket.manager.model;

import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Table `manager`
 * 
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.03
 */
public class Manager {

	private Integer managerId;
	private String managerPassword;
	private String managerName;
	private String managerEmail;
	private byte[] managerPhoto;
	private String managerStatus;

	private String creator;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime creationTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime latestEditedTime;

	/**
	 * Getter & Setter
	 */
	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getManagerPassword() {
		return managerPassword;
	}

	public void setManagerPassword(String managerPassword) {
		this.managerPassword = managerPassword;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public byte[] getManagerPhoto() {
		return managerPhoto;
	}

	public void setManagerPhoto(byte[] managerPhoto) {
		this.managerPhoto = managerPhoto;
	}

	public String getManagerStatus() {
		return managerStatus;
	}

	public void setManagerStatus(String managerStatus) {
		this.managerStatus = managerStatus;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getLatestEditedTime() {
		return latestEditedTime;
	}

	public void setLatestEditedTime(LocalDateTime latestEditedTime) {
		this.latestEditedTime = latestEditedTime;
	}
	
	 public Manager(Integer managerId, String managerPassword, String managerName, String managerEmail,
			byte[] managerPhoto, String managerStatus, String creator, LocalDateTime creationTime, LocalDateTime latestEditedTime) {
		super();
		this.managerId = managerId;
		this.managerPassword = managerPassword;
		this.managerName = managerName;
		this.managerEmail = managerEmail;
		this.managerPhoto = managerPhoto;
		this.managerStatus = managerStatus;
		this.creator = creator;
		this.creationTime = creationTime;
		this.latestEditedTime = latestEditedTime;
	}

	public Manager() {
		super();
	}

	/*
	  * Convert to String
	  */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserInfo [managerId=");
		builder.append(managerId);
		builder.append(", managerPassword=");
		builder.append(managerPassword);
		builder.append(", managerName=");
		builder.append(managerName);

		builder.append(", managerEmail=");
		builder.append(managerEmail);
		builder.append(", managerPhoto=");
		builder.append(managerPhoto);
		builder.append(", managerStatus=");
		builder.append(managerStatus);

		builder.append(", creator=");
		builder.append(creator);
		builder.append(", creationTime=");
		builder.append(creationTime);
		builder.append(", latestEditedTime=");
		builder.append(latestEditedTime);
		builder.append("]");
		return builder.toString();
	}
}

//Create table manager(
//manager_id INT NOT NULL PRIMARY KEY IDENTITY(1,1),
//manager_password VARCHAR(255) NOT NULL,
//
//manager_name NVARCHAR(10) NOT NULL,
//manager_email VARCHAR(50) NOT NULL UNIQUE,
//manager_photo VARBINARY(MAX),
//manager_status NVARCHAR(10) NOT NULL,
//
//creator NVARCHAR(10) NOT NULL,
//creation_time DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP,
//latest_edited_time DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP
//);