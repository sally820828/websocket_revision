package com.mli.websocket.manager.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mli.websocket.manager.model.Manager;

/**
 * Mapper Interface for `manager`
 * @author D2082803 Sally
 * @version 1.0
 * @since 2023.10.03
 */
@Mapper
public interface ManagerMapper {
	
	/**
	 * Login
	 * @return Manager
	 */
	Manager findManagerByEmailPassword(String managerEmail, String managerPassword);
	
	/**
	 * Insert 
	 * @return Manager
	 */
	Manager insertManager(Manager manager);
	
	/**
	 * @return List<Manager>
	 */
	List<Manager> findAllManager();
	
	/**
	 * 
	 * @return Manager
	 */
	Manager findManagerById(Integer managerId);
	
	/**
	 * 
	 * @return Manager
	 */
	Manager findManagerByIdPassword(Integer managerId, String userPassword);
	
	/**
	 * Edit
	 * @return Manager
	 */
	Manager updateManager(Manager manager);
	
	/**
	 * Delete
	 * @return boolean
	 */
	boolean deleteManager(Integer managerId);
}
