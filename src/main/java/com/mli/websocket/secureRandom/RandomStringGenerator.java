package com.mli.websocket.secureRandom;

import java.security.SecureRandom;

/**
 * RandomString For login
 * @author D2082803 Sally 
 * @version 1.0
 * @since 2023.09.25
 */

public class RandomStringGenerator {
	
	private static final String BasedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static SecureRandom secureRandom = new SecureRandom();
	
	 /**
	   * Get Reandom String 
     * @param length  
     * @return 
     */
	public static String getRandomStringByLength(int length) {
		StringBuilder stringBuilder = new StringBuilder(length);
		for(int i=0; i<length; i++) {
			int number = secureRandom.nextInt(BasedCharacters.length());
			stringBuilder.append(BasedCharacters.charAt(number));
		}
		return stringBuilder.toString();
	}
}
